import React, { Component } from 'react';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';

import PrivateRoute from './privateRoute';
import LoginPage from '../components/LoginPage';
import RegisterPage from '../components/RegisterPage';
import DashboardPage from '../components/DashboardPage';
import ProductList from '../components/ProductList';
import ProductOrder from '../components/ProductOrder';
import MyOrders from '../components/MyOrders';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
        
          <Switch>
            <Route path='/' exact={true} component={LoginPage} />
            <Route path='/login' component={LoginPage} />
            <Route path='/register' component={RegisterPage} />
            <PrivateRoute path='/dashboard' component={DashboardPage} />
            <PrivateRoute path='/productlist' component={ProductList} />
            <PrivateRoute path='/productorder' component={ProductOrder} />
            <PrivateRoute path='/myorders' component={MyOrders} />
          </Switch>

        </div>
      </BrowserRouter>
    );
  }
}

export default App;