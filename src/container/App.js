import React, { Component } from 'react';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';

import PrivateRoute from './privateRoute';
import LoginPage from '../components/LoginPage';
import RegisterPage from '../components/RegisterPage';
import DashboardPage from '../components/DashboardPage';
import WatchedList from '../components/WatchedList';



class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="container-fluid">
          <Switch>
            <Route path='/' exact={true} component={LoginPage} />
            <Route path='/login' component={LoginPage} />
            <Route path='/register' component={RegisterPage} />
            <PrivateRoute path='/dashboard' component={DashboardPage} />
            <PrivateRoute path='/WatchedList' component={WatchedList} />
            <Route path="*">
              <h2>Page Not Found!!!</h2>
            </Route>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;