import React, { Component } from 'react';
import Header from '../components/Header'
import DisplayMovieList from './DisplayMovieList';

const REQUEST_URL = 'http://localhost:4000/movie';

class DashboardPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      searchedMovie: "",
      allMovie: "",
      modalState: false
    }
  }

  componentDidMount() {
    //console.log("From DashBoard : " + contextType.serachMovieResult);
    fetch(REQUEST_URL)
      .then(response => response.json())
      .then(data => this.setState({ data }))
  }

  handleChange = (e) => {
    this.setState({ searchedMovie: e });
  }

  watchMovie = (movieId) => {
    let userId = window.sessionStorage.getItem("User_Email");
    fetch('http://localhost:2022/onlinemovie/user-movies' + userId, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "email": this.state.email,
        "password": this.state.password
      })
    })
      .then(response => response.json({
        "movieId": movieId,
        "userId": 10
      }))
      .then(data => {
        //debugger;
      })
      .catch((error) => {
        console.log(error);
      });
    this.setState({ modalState: !this.state.modalState });
  }



  handleShow = () => {
    this.setState({ modalState: !this.state.modalState });
  }

  render() {
    const playMovie = require('../movie.jfif');
    return (
      <div>
        <Header handleChangeMovie={this.handleChange} />
        {this.state.searchedMovie ?
          <>  <button onClick={(e) => this.setState({ searchedMovie: "" })}>Clear Filter</button>  <DisplayMovieList filteredmovies={this.state.searchedMovie} watchMovie={this.watchMovie} /> </> : this.state.data ? <DisplayMovieList filteredmovies={this.state.data} watchMovie={this.watchMovie} /> : ''}
        <div className={"modal fade" + (this.state.modalState ? " show d-block" : " d-none")} tabIndex="-1" role="dialog">
          <div className="modal-dialog modal-xl" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">MovieTitle</h5>
                <button type="button" className="close" onClick={this.handleShow}>
                  <span>&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <img src={playMovie} className="playMovieImg" alt="" />
              </div>
              <div className="modal-footer">
                ---
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DashboardPage;
