import React from 'react';
import { NavLink, Redirect } from 'react-router-dom'
import Search from './Search'



//Header function


const Header = (props) => {
    const handleChange =(e)=>{
        props.handleChangeMovie(e)
    }
    let username = window.sessionStorage.getItem("User_Email");
    return (
        <div className="dashboardHeader flex col-12">
        {username !== '' ? <>
            <div className="col-2">
                <NavLink className="button" to="/dashboard" exact activeClassName="success">
                    <h3>Flix</h3></NavLink>
            </div>
            <div className="col-5">
                <Search onClickItem={handleChange} />
            </div>
            <div className="col-3 text-left"><label><h6 className="userNameTop">Welcome : {username}!</h6></label></div>
            <div className="col-1 text-right">
                <NavLink className="button" to="/WatchedList" exact activeClassName="success">WatchedList</NavLink></div>

            <div className="col-1"><NavLink className="button" to="/Login" exact activeClassName="success">LogOut</NavLink></div>
        </> : <Redirect to='/Login' />}
        </div>
    )
}

export default Header;