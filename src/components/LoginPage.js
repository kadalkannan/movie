import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { setCookie } from '../utils/cookies';


class LoginPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isSuccess: false
    }
  }

  componentDidMount() {
    document.title = 'Sign in to Flix!!!';
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onHandleLogin = (event) => {
    event.preventDefault();
    // fetch('http://localhost:2022/onlinemovie/users/login', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify({
    //     "email": this.state.email,
    //     "password": this.state.password
    //   })
    // })
    //   .then(response => response.json())
    //   .then(data => {
    //     debugger;
    //     if (data.statuscode === 1000) {
    //       this.setState({ isSuccess: true });
    //       window.sessionStorage.setItem("User_Email", this.state.email);
    //       setCookie('user_email', this.state.email, 1);
    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
    this.setState({ isSuccess: true });
    window.sessionStorage.setItem("User_Email", this.state.email);
    setCookie('user_email', this.state.email, 1);

  }



  render() {
    let { isSuccess, message} = this.state;


    return (
      <div id="wrapper">
        {!isSuccess ? <div>{message}</div> : <Redirect to='dashboard' />}
        <div id="left">
          <div id="signin">
            <form onSubmit={this.onHandleLogin}>
              <div className="mt-5">
                <label>Email</label>
                <input type="text" name="email" value={this.state.email} onChange={this.handleChange} className="text-input" placeholder="Enter your Email here" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Please Enter the Valid Email!" required />
              </div>
              <div>
                <label>Password</label>
                <input type="password" name="password" value={this.state.password} onChange={this.handleChange} className="text-input" placeholder="Enter your Password here" required />
              </div>
              <button type="submit" className="primary-btn">Sign In</button>
            </form>
            <div className="or">
              <hr className="bar" />
              <span>OR</span>
              <hr className="bar" />
            </div>
            <div className="secondary-btn">
              <Link to='register'>Register here</Link>
            </div>
          </div>
          <footer id="main-footer">
            <p>Copyright &copy; 2020</p>
            <div>
              Terms of use | Privacy Policy
            </div>
          </footer>

        </div>
        <div id="right">
          <div id="showcase">
            <div className="showcase-content">
              <h1 className="showcase-text">
                Sign Up and Enjoy 24 hours <br /> Free Streeming
                    </h1>

            </div>
          </div>

        </div>
      </div>
    );
  }
}


export default LoginPage;