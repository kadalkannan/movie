import React from 'react';


// Dummy data
const REQUEST_URL = 'http://localhost:4000/movie';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      search: "",
      movie: [],
      movieType: "Movie"
    }
  }
  // fetch data
  componentDidMount() {
    fetch(REQUEST_URL)
      .then(response => response.json())
      .then(data => this.setState({ data }))
  }
  // Search input   
  onInput = e => this.setState({ [e.target.id]: e.target.value });

  // Select item
  onClickItem = item => {
    this.setState({
      search: "",
      movie: item
    });
    this.props.onClickItem(item);
  }

  changeDDL = (e) => {
    this.setState({ movieType: e.target.value });
  }

  render() {
    let { data, search } = this.state;
    if (!data) {
      return <p>Loading</p>
    }
    let filtered = data.filter(item => item.movieName.toLowerCase().includes(search.toLowerCase()));
    return (
      <div>
        <div className="wrapper1">
          <div className="search">
            <select id="movieType" onChange={this.changeDDL} value={this.state.movieType}>
              <option value="Movie">Movie</option>
              <option value="Genre">Genre</option>
            </select>
            <input
              id="search"
              type="search"
              value={this.state.search}
              placeholder={this.state.movieType === "Movie" ? "Search By Movie Name..." : "Search By Genre Name..."}
              onChange={this.onInput}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              autocomplete="off"
            />
            <i class="fas fa-search"></i>
          </div>
          {search.length > 1 && filtered.length > 0 && (
            <ul className="list">
              {filtered.map(item => (
                <li onClick={() => this.onClickItem(item)}>{item.movieName}</li>
              ))}
            </ul>
          )}
        </div>
      </div>
    )
  }
};

export default Search;