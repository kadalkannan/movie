import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import serviceURL from './serviceURL'


class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      mobile: '',
      email: '',
      password: '',
      registerType: '',
      isSuccess: false,
      message: ''
    }
  }

  componentDidMount() {
    document.title = 'Register Flix!!!';
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  onHandleRegistration = (event) => {
    event.preventDefault();
    fetch(serviceURL.register, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "email": this.state.email,
        "mobileNo": this.state.mobile,
        "password": this.state.password,
        "userName": this.state.name,
        "userType": this.state.registerType
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.statusCode === "1000") {
          alert(data.message);
          this.setState({ isSuccess: true })
        }
        else {
          alert(data.message);
          this.setState({ isSuccess: false })
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  getRegisterType = (e) => {
    debugger;
    this.setState({ registerType: e.target.value });
  }
  render() {
    let { message, isSuccess } = this.state;
    return (
      <div id="wrapper">
        {!isSuccess ? <div>{message}</div> : <Redirect to='login' />}
        <div id="left">
          <div id="signin">
            <form onSubmit={this.onHandleRegistration}>
              <div className="mt-5">
                <label>Name</label>
                <input type="text" name="name" value={this.state.name} onChange={this.handleChange} className="text-input" placeholder="Enter your Name" required />
              </div>
              <div>
                <label>Email</label>
                <input type="text" name="email" value={this.state.email} onChange={this.handleChange} className="text-input" placeholder="Enter your Email " required />
              </div>
              <div>
                <label>Mobile No</label>
                <input type="number" name="mobile" value={this.state.mobile} onChange={this.handleChange} className="text-input" placeholder="Enter your Mobile No" required />
              </div>
              <div>
                <label>Password</label>
                <input type="password" name="password" value={this.state.password} onChange={this.handleChange} className="text-input" placeholder="Enter your Password here" required />
              </div>
              <div className="mb-3">
                <label>Register As</label>
                <div onChange={this.getRegisterType}>
                  <label className="mr-3"><input type="radio" name="logintype" value="trial" />Free Trial</label>
                  <label><input type="radio" name="logintype" value="paid" />Primium User</label>
                </div>
              </div>
              <button type="submit" className="primary-btn">Register</button>
            </form>
            <span id="main-footer"> Already have account? </span><Link to='login'>Login here</Link>
          </div>

        </div>
        <div id="right">
          <div id="showcase">
            <div className="showcase-content">
              <h1 className="showcase-text">
                Sign Up and Enjoy 24 hours <br /> Free Streeming
                    </h1>
            </div>
          </div>

        </div>
      </div>

    )
  }
}


export default RegisterPage;
