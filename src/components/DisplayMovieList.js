import React from 'react';

const DisplayMovieList = (props) => {
    let movies = [];
    movies = props.filteredmovies;
    const displayLogo = require('../movie.jpg');
    const watchMovie = (e) =>
    {
      let id= e.target.id;
      props.watchMovie(id);
    }
    return (
        <div className="row displayMovie">
        <div className="grid-container col-12">
        {
          movies.length > 0 ?
          movies.map(movie => <>
            <div className="grid-item"  id={movie.movie_id} onClick={watchMovie}><img  id={movie.movie_id} className="movieImg" src={displayLogo} alt="Click to watch movie" /><br />
                Id : {movie.movie_id}<br />Name : {movie.movieName}<br />
            </div>
          </>) : ''
        }
      </div>
      </div>
    )
}

export default React.memo(DisplayMovieList);